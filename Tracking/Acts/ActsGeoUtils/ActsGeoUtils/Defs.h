/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef ACTSGEOUTILS_DEFS_H
#define ACTSGEOUTILS_DEFS_H
/// Header file to manage the common inlcudes
#include <GeoPrimitives/GeoPrimitivesHelpers.h>
///
#include <ActsGeometryInterfaces/ActsGeometryContext.h>

#include <limits>
#include <set>

#endif