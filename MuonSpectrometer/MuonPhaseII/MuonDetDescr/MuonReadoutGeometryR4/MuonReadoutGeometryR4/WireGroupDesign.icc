/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONREADOUTGEOMETRYR4_WIREGROUPDESIGN_ICC
#define MUONREADOUTGEOMETRYR4_WIREGROUPDESIGN_ICC

#include <MuonReadoutGeometryR4/WireGroupDesign.h>

namespace MuonGMR4 {
    using CheckVector2D = WireGroupDesign::CheckVector2D;
    inline Amg::Vector2D WireGroupDesign::stripPosition(int groupNum) const {
        if (groupNum >= static_cast<int>(m_groups.size())) {
            ATH_MSG_WARNING(__FILE__<<":"<<__LINE__<<" The wire group number "<<groupNum
                           <<" is out of range.");
            return Amg::Vector2D::Zero();
        }
        const wireGroup& wireGrp = m_groups[groupNum];
        return StripDesign::stripPosition(0) + 
               (wireGrp.accumlWires + wireGrp.numWires/2)*stripPitch() * stripNormal();
    }
    inline CheckVector2D WireGroupDesign::wirePosition(unsigned int groupNum, 
                                                       unsigned int wireNum) const {
       unsigned int grpIdx = groupNum - firstStripNumber();
       if (grpIdx >= m_groups.size()) {
            ATH_MSG_WARNING(__FILE__<<":"<<__LINE__<<" The wire group number "<<groupNum
                           <<"is out of range.");
            return std::nullopt;
        }
        const wireGroup& wireGrp = m_groups[grpIdx];
        if (wireNum > wireGrp.numWires){
            ATH_MSG_WARNING(__FILE__<<":"<<__LINE__<<" The wire number "
                        <<wireNum<<" is out of range. Expect in group "<<groupNum<<
                        " [1-"<<wireGrp.numWires<<"] wires.");
            return std::nullopt;
        }
        return std::make_optional<Amg::Vector2D>(StripDesign::stripPosition(0) +  
                                                 (wireGrp.accumlWires + (wireNum - 1))*stripPitch() * stripNormal());
    }
    inline CheckVector2D WireGroupDesign::leftWireEdge(unsigned int groupNum,
                                                       unsigned int wireNum) const {
        CheckVector2D wPos{wirePosition(groupNum, wireNum)};
        if(!wPos) return std::nullopt;
        return leftInterSect(*wPos);
    }
        
    inline CheckVector2D WireGroupDesign::rightWireEdge(unsigned int groupNum,
                                                        unsigned int wireNum) const {
        CheckVector2D wPos{wirePosition(groupNum, wireNum)};
        if(!wPos) return std::nullopt;
        return rightInterSect(*wPos);
    }
    inline double WireGroupDesign::wireLength(unsigned int groupNum,
                                              unsigned int wireNum) const{
        CheckVector2D wirePos{}, lIsect{}, rIsect{};
        if (!(wirePos = wirePosition(groupNum, wireNum)) ||
            !(lIsect = leftInterSect(*wirePos)) ||
            !(rIsect = rightInterSect(*wirePos))){
             return 0;
        }        
        return ((*lIsect)- (*rIsect)).mag();
    }

    inline int WireGroupDesign::stripNumber(const Amg::Vector2D& extPos) const {
        std::optional<double> wirePitches = Amg::intersect<2>(StripDesign::stripPosition(0), stripDir(),
                                                               extPos, - Amg::Vector2D::UnitX());
        if (!wirePitches) {
            return -1;
        }
        const double distFromFirst = (*wirePitches);        
        /// The external position is waaay before the first wire position
        if (distFromFirst < 0 || !insideTrapezoid(extPos)){
            return -1;
        }
        /// Find the first group whose first last wire is encapsulating the distance
        wireGrpVectorItr strip_itr = std::upper_bound(m_groups.begin(), m_groups.end(), 
                                                      distFromFirst,
                                        [this]( const double dist, const wireGroup& grp) {
                                            const double firstWire = stripPitch() *(grp.accumlWires -0.5);
                                            return dist < firstWire;
                                        });
        if (strip_itr == m_groups.begin()) {
            return -1;
        }
        return std::distance(m_groups.begin(), strip_itr) -1 + firstStripNumber();
    }
    inline std::pair<int, int> WireGroupDesign::wireNumber(const Amg::Vector2D& extPos) const {
        const int wireGrp = stripNumber(extPos);
        if (wireGrp < 0) {
            return std::make_pair(wireGrp, wireGrp);
        }        
        std::optional<double> wirePitches = Amg::intersect<2>(wirePosition(wireGrp, 1).value(), stripDir(),
                                                               extPos, - Amg::Vector2D::UnitX());
        const int wire = std::round(wirePitches.value_or(-1.) / stripPitch()) +1;
        return std::make_pair(wireGrp, wire);
    }
    


}
#endif