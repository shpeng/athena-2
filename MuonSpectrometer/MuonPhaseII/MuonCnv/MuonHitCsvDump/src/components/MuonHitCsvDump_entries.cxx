
/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../MdtDriftCircleCsvDumperAlg.h"
#include "../MuonSimHitCsvDumperAlg.h"
#include "../MuonStripCsvDumperAlg.h"

DECLARE_COMPONENT(MdtDriftCircleCsvDumperAlg)
DECLARE_COMPONENT(MuonSimHitCsvDumperAlg)
DECLARE_COMPONENT(MuonStripCsvDumperAlg)
